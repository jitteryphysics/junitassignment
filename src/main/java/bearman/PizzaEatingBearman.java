package bearman;

import food.Food;
import food.Pizza;

public class PizzaEatingBearman extends Bearman{
	public void eat(Food p) {
		p.feedBearman(this);
	}
}

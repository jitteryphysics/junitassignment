package bearman;

import food.Food;
import food.Taco;

public class TacoEatingBearman extends Bearman{
	public void eat(Food t) {
		t.feedBearman(this);
	}
}
